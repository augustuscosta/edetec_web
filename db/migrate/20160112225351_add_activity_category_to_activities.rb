class AddActivityCategoryToActivities < ActiveRecord::Migration
  def change
    add_column :activities, :category_id, :string
  end
end
