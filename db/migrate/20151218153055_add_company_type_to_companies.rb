class AddCompanyTypeToCompanies < ActiveRecord::Migration
  def change
    add_reference :companies, :phase, index: true, foreign_key: true
  end
end
