class ChangePhaseColumn < ActiveRecord::Migration
  def change
  	rename_column :company_phases, :phase, :name
  end
end
