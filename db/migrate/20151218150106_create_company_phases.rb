class CreateCompanyPhases < ActiveRecord::Migration
  def change
    create_table :company_phases do |t|
      t.string :phase

      t.timestamps null: false
    end
  end
end
