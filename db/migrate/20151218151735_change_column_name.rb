class ChangeColumnName < ActiveRecord::Migration
  def change
  	rename_column :company_types, :type, :name
  end
end
