class AddCompanyToActivities < ActiveRecord::Migration
  def change
  	add_reference :activities, :company, index: true
  end
end
