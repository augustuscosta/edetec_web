class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :fantasy_name
      t.string :company_name
      t.string :cnpj
      t.string :description
      t.string :cep
      t.string :street
      t.string :area
      t.string :complement
      t.string :city
      t.string :state
      t.string :phone
      t.string :email
      t.string :facebook
      t.string :twitter

      t.timestamps null: false
    end
  end
end
