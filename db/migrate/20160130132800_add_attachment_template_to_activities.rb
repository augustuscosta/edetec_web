class AddAttachmentTemplateToActivities < ActiveRecord::Migration
  def self.up
    change_table :activities do |t|
      t.attachment :template
    end
  end

  def self.down
    remove_attachment :activities, :template
  end
end
