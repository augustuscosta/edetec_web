class AddCompanyPhaseToCompanies < ActiveRecord::Migration
  def change
    add_reference :companies, :type, index: true, foreign_key: true
  end
end
