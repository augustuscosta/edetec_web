class AddAttachmentResultToActivities < ActiveRecord::Migration
  def self.up
    change_table :activities do |t|
      t.attachment :result
    end
  end

  def self.down
    remove_attachment :activities, :result
  end
end
