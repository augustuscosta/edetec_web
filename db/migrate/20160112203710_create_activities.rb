class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.string :name
      t.string :description
      t.date :start_at
      t.date :end_at

      t.timestamps null: false
    end
  end
end
