# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

['ADMIN', 'NORMAL', 'GUEST'].each do |role|
  Role.find_or_create_by({name: role})
end

['Categoria 1', 'Categoria 2', 'Categoria 3'].each do |name|
  ActivityCategory.find_or_create_by({name: name})
end

['Tipo 1', 'Tipo 2', 'Tipo 3'].each do |name|
  CompanyType.find_or_create_by({name: name})
end

['Fase 1', 'Fase 2', 'Fase 3'].each do |name|
  CompanyPhase.find_or_create_by({name: name})
end