class UsersController < ApplicationController
  include ApplicationHelper
  before_filter :authenticate_user!, :authenticate_admin
  def index
    @users = User.all
  end

  def edit
    @user = User.find(params[:id])
  end

  def delete_user
    @deleted_user = User.find(params[:id])
    @deleted_user.destroy
    redirect_to :action => :index
  end

  def update
    @user = User.find(params[:id])
    @user.companies =  params[:user][:companies].empty? ? [] : [Company.find(params[:user][:companies])]

    if @user.update_attributes(params.require(:user).permit(:role_id))
      flash[:success] = 'Usuário editado com sucesso'
    else
      flash[:notice] = "Não foi possível editar usuário"
    end
    redirect_to action: "index"
  end

  def authenticate_admin
    if !isAdmin?
      redirect_to root_path
    end
  end
end
