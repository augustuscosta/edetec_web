class CompaniesController < ApplicationController
  include ApplicationHelper
  before_action :set_company, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!
  before_filter :authenticate_admin, only: [:create, :new, :update, :destroy]

  # GET /companies
  # GET /companies.json
  def index
    @companies = Company.all
  end

  # GET /companies/1
  # GET /companies/1.json
  def show
  end

  # GET /companies/new
  def new
    @company = Company.new
  end

  # GET /companies/1/edit
  def edit
  end

  # POST /companies
  # POST /companies.json
  def create
    @company = Company.new(company_params)

    respond_to do |format|

      @companyUsers = []
      if params[:company][:users] != nil
        for userId in params[:company][:users]
          if userId != ""
            user = User.find(userId)
            if user.companies.empty?
              @companyUsers.push(User.find(userId))
            end
          end
        end
      end
      @company.users = @companyUsers

      if @company.save
        format.html { redirect_to @company, notice: 'Company was successfully created.' }
        format.json { render :show, status: :created, location: @company }
      else
        format.html { render :new }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /companies/1
  # PATCH/PUT /companies/1.json
  def update
    respond_to do |format|

      @companyUsers = []
      if params[:company][:users] != nil
        for userId in params[:company][:users]
          if userId != ""
            user = User.find(userId)
            if user.companies.empty?
              @companyUsers.push(User.find(userId))
            end
          end
        end
      end
      @company.users = @companyUsers
      if @company.update(company_params)
        format.html { redirect_to @company, notice: 'Company was successfully updated.' }
        format.json { render :show, status: :ok, location: @company }
      else
        format.html { render :edit }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /companies/1
  # DELETE /companies/1.json
  def destroy
    @company.destroy
    respond_to do |format|
      format.html { redirect_to companies_url, notice: 'Company was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company

      @company = Company.find(params[:id])

      @activitiesCompleted = 0
      @activitiesPending = 0

      for activity in @company.activities
        if (!activity.result_file_size.nil?)
          @activitiesCompleted += 1
        else
          @activitiesPending += 1
        end
      end

      @users = []
      User.order(:id)
      for user in User.order(:id)
        if user.companies.empty? || user.companies.first.id == @company.id
          @users.push(user)
        end
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_params
      params.require(:company).permit(:logo, :fantasy_name, :company_name, :cnpj, :description, :cep, :street, :area, :complement, :city, :state, :phone, :email, :facebook, :twitter, :type_id, :phase_id)
    end

    def authenticate_admin
      if !isAdmin?
        redirect_to root_path
      end
    end

end
