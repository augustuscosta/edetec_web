class ActivityCategory < ActiveRecord::Base
  has_many :activities, :foreign_key => 'category_id'
end
