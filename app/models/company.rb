class Company < ActiveRecord::Base
  has_and_belongs_to_many :users
  belongs_to :company_phase, :foreign_key => 'phase_id'
  belongs_to :company_type, :foreign_key => 'type_id'
  has_many :activities
  has_attached_file :logo, :styles => { :medium => "300x300>", :thumb => "100x100#" }, :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :logo, :content_type => /\Aimage\/.*\Z/
end
