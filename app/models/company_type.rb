class CompanyType < ActiveRecord::Base
	has_many :companies, :foreign_key => 'type_id'
end
