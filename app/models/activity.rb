class Activity < ActiveRecord::Base
  belongs_to :activity_category, :foreign_key => 'category_id'
  belongs_to :company

  has_attached_file :template
  has_attached_file :result
  validates_attachment :template, :content_type => { :content_type => %w(application/pdf application/msword application/vnd.openxmlformats-officedocument.wordprocessingml.document) }
  validates_attachment :result, :content_type => { :content_type => %w(application/pdf application/msword application/vnd.openxmlformats-officedocument.wordprocessingml.document) }
end
