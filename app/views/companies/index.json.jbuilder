json.array!(@companies) do |company|
  json.extract! company, :id, :fantasy_name, :company_name, :cnpj, :description, :cep, :street, :area, :complement, :city, :state, :phone, :email, :facebook, :twitter
  json.url company_url(company, format: :json)
end
