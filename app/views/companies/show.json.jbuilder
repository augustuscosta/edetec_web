json.extract! @company, :id, :fantasy_name, :company_name, :cnpj, :description, :cep, :street, :area, :complement, :city, :state, :phone, :email, :facebook, :twitter, :created_at, :updated_at
